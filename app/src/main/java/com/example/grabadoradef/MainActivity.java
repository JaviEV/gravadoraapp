package com.example.grabadoradef;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO};

    private ImageView logo;
    private ImageButton record,stop,play;
    private MediaRecorder mRecorder = null;
    private MediaPlayer mPlayer = null;
    private String fileName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(this, permissions,
                REQUEST_RECORD_AUDIO_PERMISSION);

        logo = findViewById(R.id.imageView);
        record = findViewById(R.id.imageButton2);
        stop = findViewById(R.id.imageButton3);
        play = findViewById(R.id.imageButton4);
        String ruta = this.getExternalFilesDir(null).getAbsolutePath();
        fileName = ruta + "/audiorecord.3gp";




        record.setOnClickListener(v -> {
            logo.setImageDrawable(
                    getResources().getDrawable(R.drawable.recording)
            );
            Toast.makeText(this, "Gravant..", Toast.LENGTH_SHORT).show();
            if (mRecorder == null){
                mRecorder = new MediaRecorder();
                mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                mRecorder.setOutputFile(fileName);
                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                try {
                    mRecorder.prepare();
                }catch (IOException e){
                    Log.e("RECORDING", "No es pot iniciar la gravació");
                }
                mRecorder.start();
            }
        });

        stop.setOnClickListener(v -> {
            logo.setImageDrawable(
                    getResources().getDrawable(R.drawable.logo)
            );
            Toast.makeText(this, "Parat", Toast.LENGTH_SHORT).show();
            if (mRecorder != null) {
                mRecorder.stop();
                mRecorder.reset();
                mRecorder.release();
                mRecorder = null;
            } else if (mPlayer != null) {
                mPlayer.stop();
                mPlayer.release();
                mPlayer = null;
            }
        });

        play.setOnClickListener(v -> {
            logo.setImageDrawable
                    (getResources().getDrawable(R.drawable.playing)
            );
            Toast.makeText(this, "Reproduint..", Toast.LENGTH_SHORT).show();
            if (mRecorder != null && mPlayer == null ){
                mPlayer = new MediaPlayer();
                try {
                    mPlayer.setDataSource(fileName);
                    mPlayer.prepare();
                    mPlayer.start();
                    mPlayer.setOnCompletionListener(mediaPlayer -> {
                        stop.callOnClick();
                    });

                }catch (IOException e){
                    Log.e("RECORDING", "La reproducció no es pot iniciar");
                }

            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] != PackageManager.PERMISSION_DENIED;
                break;
        }
        if (!permissionToRecordAccepted){
            Toast.makeText(this,"Permission needed",Toast.LENGTH_LONG).show();
            this.finish();
        }
    }
}
